#ifndef WIDGETFILEINFO_H
#define WIDGETFILEINFO_H

#include <QWidget>
#include <QTextBrowser>

namespace Ui {
class WidgetFileInfo;
}

class WidgetFileInfo : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetFileInfo(QWidget *parent = 0);
    ~WidgetFileInfo();    
    QTextBrowser* getTextBrowser();
private:
    Ui::WidgetFileInfo *ui;
};

#endif // WIDGETFILEINFO_H
