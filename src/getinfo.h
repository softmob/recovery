#ifndef GETINFO_H
#define GETINFO_H

#include <QtGlobal>
#include <QByteArray>
#include <qt_windows.h>

struct getInfo
{    
    static long unsigned int sector;
    static long unsigned int cluster;
    static long long unsigned int bytoToCluster;
    static long unsigned int sizeFR;
    static HANDLE hFile;
    static char drive;
    static QByteArray MftBitmap;
    static QByteArray Bitmap;
};

#endif // GETINFO_H
