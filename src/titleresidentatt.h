#ifndef TITLERESIDENTATT_H
#define TITLERESIDENTATT_H

#include <QDataStream>

struct TitleResidentAtt
{
    //Размер области данных
    quint32 size;
    //Смещение области данных от начала атрибута
    quint16 off;
    //Ещё какие-то флаги
    quint16 flag;
};

QDataStream & operator>> (QDataStream &, TitleResidentAtt&);

#endif // TITLERESIDENTATT_H
