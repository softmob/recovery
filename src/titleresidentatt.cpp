#include "titleresidentatt.h"

QDataStream & operator>> ( QDataStream & in, TitleResidentAtt& value)
{
    in >> value.size >> value.off >> value.flag;
    return in;
}
