#ifndef TITLEATT_H
#define TITLEATT_H

#include <QtGlobal>
#include <QDataStream>

struct TitleAtt
{
    //Тип атрибута
    quint32 type;
    //Размер атрибута, включая заголовок
    quint32 size;
    /*Флаг: 0 - атрибут резидентный
            1 - атрибут нерезидентный*/
    quint8 flag;
    //Длина имени атрибута
    quint8 len;
    //Смещение имени атрибута
    quint16 off;
    /*Флаги :
        бит 0 - данные атрибута упакованы*/
    quint16 rar;
    //Номер атрибута по порядку
    quint16 num;
};

QDataStream& operator>> (QDataStream&, TitleAtt&);

#endif // TITLEATT_H
