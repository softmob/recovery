#ifndef ATTRIBUTELIST_H
#define ATTRIBUTELIST_H

#include <QtGlobal>
#include <QDataStream>

struct AttributeList
{
    // 00h	4	тип (type) атрибута (см. таблицу 6)
    quint32 type;
    //04h	2	длина записи (record length)
    quint16 rec_size;
    // 06h	1	длина имени (name length), или ноль, если нет; условно - N
    quint8 name_len;
    // 07h	1	смещение имени (offset to name), или ноль если нет
    quint8 name_off;
    // 08h	8	начальный виртуальный кластер (starting VCN)
    quint64 vcn;
    //10h	8	ссылка на базовую/расширенную файловую запись
    quint64 file_ref;
    // 18h	2	идентификатор атрибута (attribute ID)
    quint16 id;
    // 1Ah	2N	if N > 0, то имя в формате UNICODE
};

QDataStream& operator>> (QDataStream&, AttributeList&);

#endif // ATTRIBUTELIST_H
