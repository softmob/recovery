#-------------------------------------------------
#
# Project created by QtCreator 2013-12-13T11:25:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++0x

TARGET = recovery
TEMPLATE = app

RC_FILE = myapp.rc

QTPLUGIN += qico

SOURCES += main.cpp\
        mainwindow.cpp \
    filerecord.cpp \
    runs.cpp \
    titleatt.cpp \
    titleresidentatt.cpp \
    titlenoresidentatt.cpp \
    titlemft.cpp \
    utility.cpp \
    init.cpp \
    getinfo.cpp \
    mftruns.cpp \
    control.cpp \
    driveinfo.cpp \
    widgetfileinfo.cpp \
    widgetheadhex.cpp \
    file.cpp \
    attributelist.cpp

HEADERS  += mainwindow.h \
    filerecord.h \
    runs.h \
    titleatt.h \
    titleresidentatt.h \
    titlenoresidentatt.h \
    titlemft.h \
    utility.h \
    title.h \
    init.h \
    getinfo.h \
    mftruns.h \
    control.h \
    driveinfo.h \
    widgetfileinfo.h \
    widgetheadhex.h \
    file.h \
    attributelist.h

FORMS    += mainwindow.ui \
    widgetfileinfo.ui \
    widgetheadhex.ui

RESOURCES += \
    resources.qrc
