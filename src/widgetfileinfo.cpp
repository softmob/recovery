#include "widgetfileinfo.h"
#include "ui_widgetfileinfo.h"

WidgetFileInfo::WidgetFileInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetFileInfo)
{
    ui->setupUi(this);

}

WidgetFileInfo::~WidgetFileInfo()
{
    delete ui;
}

QTextBrowser *WidgetFileInfo::getTextBrowser()
{
    return  ui->textBrowser;
}
