#ifndef WIDGETHEADHEX_H
#define WIDGETHEADHEX_H

#include <QWidget>
#include <QTextBrowser>

namespace Ui {
class WidgetHeadHex;
}

class WidgetHeadHex : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetHeadHex(QWidget *parent = 0);
    ~WidgetHeadHex();
    QTextBrowser* getTextBrowser();

private:
    Ui::WidgetHeadHex *ui;
};

#endif // WIDGETHEADHEX_H
