#ifndef FILERECORD_H
#define FILERECORD_H

#include <QDataStream>
#include <QtAlgorithms>
#include <QVector>
#include <QSet>
#include "title.h"
#include "utility.h"
#include "getinfo.h"
#include "attributelist.h"
#include "init.h"
#include "file.h"


class FileRecord
{
public:
    FileRecord(const char *, quint16 = 0x400, bool del = true);
    FileRecord(QByteArray, bool del = true);
    QString getName() const;
    QString getPath();
    QVector<Runs> getRunsData() const;
    QByteArray getByteData() const;
    bool EmptyRunsData() const;
    bool EmptyByteData() const;
    bool isFileRecord() const;
    bool isDelete() const;
    bool isBase() const;
    bool isRight() const;
    quint64 getParentFile() const;
    bool isDir() const;
    quint64 getLength() const;
    quint64 getTime() const;

private:
    void parseBitmap(quint32, QByteArray&);
    void parseBitmap(QVector<Runs>, QByteArray&);
    void buildPath();
    QSet<quint64> parseAttList(quint64);
    QSet<quint64> parseAttList(QVector<Runs>&, quint64);
    void pasreOtherFR(QSet<quint64>&);
    QVector<Runs> buildRuns();
    bool recoveryRecord(TitleMft&);
    void parseNtfsFileRecord();
    void parseFileName();
private:
    QByteArray buf;
    QDataStream in;
    QVector<Runs> v;
    bool m_isFile;
    bool m_isDelete;
    bool m_isBase;
    bool m_type_del;
    bool m_BuildPath;
    bool m_isDir;
    quint64 m_parentFile;
    QString m_file_name;
    QString m_filePath;
    QByteArray bufData;
    quint64 m_length;
    quint64 m_time;
};

#endif // FILERECORD_H
