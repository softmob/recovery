#ifndef FILE_H
#define FILE_H

#include <QString>
#include "title.h"
#include "runs.h"

struct File
{
    File();
    QString name;
    QString path;
    quint64 size;
    QVector<Runs> data_list;
    QByteArray data_byte;
    quint64 length;
    QString time;
};

#endif // FILE_H
