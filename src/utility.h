#ifndef UTILITY_H
#define UTILITY_H

#include <QtCore>
#include <QString>
#include <QVector>
#include <QDataStream>
#include <QDir>
#include <qt_windows.h>
#include "driveinfo.h"
#include "runs.h"

#include <QProgressDialog>
#include <QDateTime>

class FileRecord;

QString getStringASCII(QDataStream & , quint16 );
QString getStringUTF16(QDataStream & , quint16 );
quint64 getByteN(QDataStream &, quint8 );

quint64 getByteN(const char *, quint8 );

bool DiskOpen(QString path, HANDLE& hFile);

void DiskClose(HANDLE& hFile);

QVector<DriveInfo> getDriveNTFS();

quint64 FileRefToId(quint64);

QString getSystemDrive();

LARGE_INTEGER GetFilePointerEx (HANDLE);

QByteArray getByteFileRecord(quint64);

bool valueMftBitMap(quint32);

bool valueBitMap(quint32);

bool getBitInByte(quint8, quint8);

QString ConvertFileSize(quint64);

QString convertToUtc(quint64);

std::pair<quint64, quint64> checkClusterFile(QVector<Runs>&);

QString statusFile(std::pair<quint64, quint64>);

QByteArray getSector(quint64);
QByteArray getCluster(quint64);

QString hexToString(QByteArray&);

class File;
bool recoveryFile(File, QString, QProgressDialog&);

quint64 getDriveSize(char);

#endif // UTILITY_H
