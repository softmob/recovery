#ifndef TITLENORESIDENTATT_H
#define TITLENORESIDENTATT_H

#include <QDataStream>

struct TitleNoResidentAtt
{
    // 10h	8	Первый виртуальный кластер (VCN) данных атрибута
    quint64 vcn;
    //18h	8	Последний виртуальный кластер данных
    quint64 vcn_last;
    //20h	2	Смещение списка экстентов от начала атрибута
    quint16 off;
    //22h	2	 	размер блока сжатия (compression unit size), округленный до 4 байт вверх
    quint16 rar;
    // 24h	4	00h	для выравнивания
    quint32 _nop;

    // 28h	8	 	выделенный размер (allocated size), округленный до размера кластера
    quint64 data_size;
    // 30h	8	 	реальный размер (real size)
    quint64 data_size_real;
    // 38h	8	 	инициализированный размер потока (initialized data size of the stream)
    quint64 data_stream;
};

QDataStream & operator>> ( QDataStream&, TitleNoResidentAtt&);

#endif // TITLENORESIDENTATT_H
