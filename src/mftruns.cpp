#include "mftruns.h"

MftRuns &MftRuns::Instance()
{
    static MftRuns singleton;
    return singleton;
}

void MftRuns::setDrive(QVector<Runs> runs)
{
    RunList = runs;
    m_size = 0;
    for (quint32 i = 0; i < (quint32)RunList.size(); ++i)
    {
        maps[m_size] = RunList[i].first;
        key_set.insert(m_size);
        m_size += RunList[i].len * getInfo::bytoToCluster / getInfo::sizeFR;
    }
}

quint64 MftRuns::getOffset(quint32 id) const
{
    if (id >= m_size)
        return -1;
    auto it = key_set.lower_bound(id);
    if (*it != id)
        --it;
    quint32 st = *it;
    quint64 first = maps[st];
    st = id - st;
    quint64 ret = first * getInfo::bytoToCluster + st * getInfo::sizeFR;
    return ret;
}

quint32 MftRuns::size() const
{
    return m_size;
}
