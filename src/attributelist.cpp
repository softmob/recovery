#include "attributelist.h"

QDataStream & operator>> (QDataStream & in, AttributeList& value)
{
    in >> value.type
       >> value.rec_size
       >> value.name_len
       >> value.name_off
       >> value.vcn
       >> value.file_ref;
    return in;
}
