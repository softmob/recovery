#include "filerecord.h"

FileRecord::FileRecord(const char * str, quint16 strSize, bool del): buf(str, strSize),
    in(&buf, QIODevice::ReadWrite),
    m_isFile(false), m_isDelete(false), m_isBase(false), m_parentFile(0), m_type_del(del),
    m_BuildPath(false), m_isDir(false)
{
    in.setByteOrder(QDataStream::LittleEndian);
    parseNtfsFileRecord();
}

FileRecord::FileRecord(QByteArray _buf, bool del): FileRecord(_buf.data(), _buf.size(), del)
{}

bool FileRecord::isFileRecord() const
{
    return m_isFile;
}

QString FileRecord::getName() const
{
    return m_file_name;
}

QString FileRecord::getPath()
{
    if (!m_BuildPath)
    {
        this->buildPath();
        m_BuildPath = true;
    }
    return m_filePath;
}

QVector<Runs> FileRecord::buildRuns()
{
    QVector<Runs> res;
    while (true)
    {
        quint8 byte;
        in >> byte;
        if (byte == 0)
            break;
        quint8 l = byte / 16;
        quint8 s = byte & 15;
        qint64 len = getByteN(in, s);
        qint64 first = getByteN(in, l);
        if (!res.empty())
            first += res.back().first;
        res.push_back( Runs( first, len ) );
    }
    return res;
}

bool FileRecord::recoveryRecord(TitleMft& mft)
{
    qint64 pos = in.device()->pos();
    in.device()->seek(mft.arr_off);
    quint16 fix_up;
    in >> fix_up;
    for (quint16 i = 0; i < mft.arr_size - 1; ++i)
    {
        quint16 fix_new;
        in >> fix_new;

        qint64 pos_cur = in.device()->pos();
        in.device()->seek(getInfo::sector * (i+1) - 2);
        quint16 fix_cur;
        in >> fix_cur;
        if (fix_cur != fix_up)
            return false;
        in.device()->seek(getInfo::sector * (i+1) - 2);
        in << fix_new;
        in.device()->seek(pos_cur);
    }
    in.device()->seek(pos);
    return true;
}

void FileRecord::parseNtfsFileRecord()
{
    TitleMft mft;
    in >> mft;
    m_isBase = (mft.file_ref == 0);
    if (mft.Signature == "FILE")
        m_isFile = recoveryRecord(mft);
    if (!isFileRecord())
        return ;
    m_isDelete = (mft.flag == 0);
    m_isDir = (mft.flag != 1) && (mft.flag != 0);

    if (!m_isDelete && m_type_del == true)
        return ;

    in.device()->seek(mft.off_first_att);

    while (true)
    {
        qint64 pos = in.device()->pos();

        TitleAtt head;
        in >> head;
        if (head.type == 0x0FFFFFFFF)
            break;

        if (head.flag == 0)
        {
            TitleResidentAtt headResAtt;
            in >> headResAtt;
            in.device()->seek(pos+headResAtt.off);

            if (head.type == 0x30)
                parseFileName();

            if (head.type == 0x80 && head.len == 0)
            {
                qint64 pos = in.device()->pos();
                bufData.resize(headResAtt.size);
                for (quint32 si = pos, di = 0; si < pos + headResAtt.size; ++di, ++si)
                {
                    bufData[di] = buf[si];
                }
                m_length = bufData.size();
            }

            if (head.type == 0x20)
            {
                ;//pasreOtherFR(parseAttList(headResAtt.size));
            }

            if (head.type == 0xB0 && mft.index == 0)
            {
                this->parseBitmap(headResAtt.size, getInfo::MftBitmap );
            }
            if (head.type == 0x80 && mft.index == 6)
            {
                getInfo::Bitmap = bufData;
            }

            if (head.type == 0x10)
            {
                in >> m_time >> m_time;
            }
        }
        else
        {
            TitleNoResidentAtt headNoResAtt;
            in >> headNoResAtt;
            in.device()->seek(pos+headNoResAtt.off);

            if (head.type == 0x80 && head.len == 0)
            {
                v = buildRuns();
                m_length = headNoResAtt.data_size_real;
            }

            if (head.type == 0x20)
            {
                ;// pasreOtherFR(parseAttList(buildRuns(),headNoResAtt.att_size ));
            }

            if (head.type == 0xB0 && mft.index == 0)
            {
                this->parseBitmap(buildRuns(), getInfo::MftBitmap );
            }
            if (head.type == 0x80 && mft.index == 6)
            {
                this->parseBitmap(v, getInfo::Bitmap );
            }
        }
        in.device()->seek(pos+head.size);
    }
    return ;
}

QVector<Runs> FileRecord::getRunsData() const
{
    return v;
}

QByteArray FileRecord::getByteData() const
{
    return bufData;
}

bool FileRecord::EmptyRunsData() const
{
    return v.empty();
}

bool FileRecord::EmptyByteData() const
{
    return bufData.size() == 0;
}

bool FileRecord::isDelete() const
{
    return m_isDelete;
}

void FileRecord::parseFileName()
{
    qint64 pos = in.device()->pos();
    in >> m_parentFile;
    m_parentFile = FileRefToId(m_parentFile);

    in.device()->seek(pos+0x40);
    quint8 len, spases;
    in >> len >> spases;
    if (m_file_name.isEmpty() || spases != 2)
        m_file_name = getStringUTF16(in, len);
    in.device()->seek(pos);
}
quint64 FileRecord::getTime() const
{
    return m_time;
}

quint64 FileRecord::getLength() const
{
    return m_length;
}

bool FileRecord::isDir() const
{
    return m_isDir;
}

bool FileRecord::isBase() const
{
    return m_isBase;
}

bool FileRecord::isRight() const
{
    return m_isBase && m_isDelete && m_isFile;
}

quint64 FileRecord::getParentFile() const
{
    return m_parentFile;
}

void FileRecord::parseBitmap(quint32 size, QByteArray& out)
{
    QByteArray res;
    res.resize(size);
    for (quint32 i = 0; i < size; ++i)
    {
        quint8 ch;
        in >> ch;
        res[i] = ch;
    }
    out = res;
}

void FileRecord::parseBitmap(QVector<Runs> r, QByteArray& out)
{
    QByteArray res;
    char *buf = new char[getInfo::bytoToCluster];
    LARGE_INTEGER offset;
    DWORD read;
    for (int i = 0; i < r.size(); ++i)
    {
        offset.QuadPart = r[i].first * getInfo::bytoToCluster;
        SetFilePointerEx(getInfo::hFile, offset, NULL, FILE_BEGIN);
        for (int j = 0; j < r[i].len; ++j)
        {
            read = 0;
            ReadFile(getInfo::hFile, buf, getInfo::bytoToCluster, &read, NULL);
            QByteArray tmp(buf, read);
            res += tmp;
        }
    }
    out = res;
    delete [] buf;
}

void FileRecord::buildPath()
{
    if (m_parentFile == 0)
        return ;
    QString res;
    quint64 cur = m_parentFile, next;
    while (true)
    {
        FileRecord tmp(getByteFileRecord(cur), false);
        next = tmp.getParentFile();
        if (cur == m_parentFile && next == 5)
            break;

        if (next == cur || tmp.isDir() == false)
        {
            res = "?\\" + res;
            break;
        }

        res = tmp.getName() + "\\" + res;
        cur = next;

        if (next == 5)
            break;
    }
    m_filePath.sprintf("%c:\\", getInfo::drive);
    m_filePath += res;
}

QSet<quint64> FileRecord::parseAttList(quint64 size)
{
    qint64 p = in.device()->pos();
    QSet<quint64> res;
    quint64 read = 0;
    while (read < size)
    {
        qint64 cur = in.device()->pos();
        AttributeList tmp;
        in >> tmp;
        res.insert(FileRefToId(tmp.file_ref));
        in.device()->seek(cur + tmp.rec_size);
        read += tmp.rec_size;
    }
    in.device()->seek(p);
    return res;
}

QSet<quint64> FileRecord::parseAttList(QVector<Runs>&, quint64 size)
{
    LARGE_INTEGER cur = GetFilePointerEx(getInfo::hFile);
    char *buf = new char[size];
    DWORD r = 0;
    ReadFile(getInfo::hFile, buf,size, &r, NULL);
    QDataStream file(QByteArray(buf, r));

    QSet<quint64> res;
    quint64 read = 0;
    while (read < size)
    {
        qint64 curStream = file.device()->pos();
        AttributeList tmp;
        file >> tmp;
        res.insert(FileRefToId(tmp.file_ref));
        file.device()->seek(curStream + tmp.rec_size);
        read += tmp.rec_size;
    }

    SetFilePointerEx(getInfo::hFile, cur, NULL, FILE_BEGIN);
    delete [] buf;
    return res;
}

void FileRecord::pasreOtherFR(QSet<quint64>& list)
{
    DWORD read = 0;
    char *buf = new char[getInfo::sizeFR];
    for (auto &x: list)
    {
        quint64 off = MftRuns::Instance().getOffset(x);
        LARGE_INTEGER offset;
        offset.QuadPart = off;
        SetFilePointerEx(getInfo::hFile, offset, NULL, FILE_BEGIN);
        ReadFile(getInfo::hFile, buf, getInfo::sizeFR, &read, NULL);
        FileRecord next(buf, read);
        if (!next.EmptyRunsData())
            v = next.getRunsData();
        if (!next.EmptyByteData())
            bufData = next.getByteData();
        if (this->getName().size() == 0)
            m_file_name = next.getName();
        if (this->getPath().size() == 0)
            m_file_name = next.getPath();
    }
    delete [] buf;
}
