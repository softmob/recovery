#ifndef MFTRUNS_H
#define MFTRUNS_H

#include <QDataStream>
#include <QString>
#include <QVector>
#include <QHash>
#include <set>
#include "utility.h"
#include "filerecord.h"
#include "title.h"

class MftRuns
{
public:
    static MftRuns &Instance();
    void setDrive(QVector<Runs> runs);
    quint64 getOffset(quint32) const;
    quint32 size() const;
private:
    QVector<Runs> RunList;
    quint32 m_size;
    QHash<qint32, qint64> maps;
    std::set<quint32> key_set;
private:
    MftRuns(){}
    MftRuns(const MftRuns&);
    MftRuns& operator=(const MftRuns&);
};

#endif // MFTRUNS_H
