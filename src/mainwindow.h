#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

#include <QDirModel>
#include <QString>
#include <qt_windows.h>
#include <QFileSystemModel>
#include <string>
#include <QMainWindow>
#include "driveinfo.h"
#include "utility.h"
#include "control.h"
#include <QCheckBox>
#include <QSplitter>
#include <QHash>
#include <QMessageBox>
#include <QFileDialog>
#include <cctype>
#include <QRegExp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void tableFill(QVector<File>);
    void recovery(QList<QTableWidgetItem*>);
private slots:
    void about();
    void help();
    void recoverySelect();
    void recoveryChecked();
    void selectAdd();
    void selectDel();

    void selectAllUncheck();
    void selectAllCheck();

    void on_pushScan_clicked();

    void on_table_doubleClicked(const QModelIndex &index);

    void on_table_itemSelectionChanged();

    void on_table_customContextMenuRequested(const QPoint &pos);

    void on_pushRecovery_clicked();

    void on_comboBox_activated(const QString &arg1);

    void on_filter_textEdited(const QString &arg1);

private:
    QHash<QTableWidgetItem*, File> map;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H



