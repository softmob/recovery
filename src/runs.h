#ifndef RUNS_H
#define RUNS_H

#include <QtGlobal>

struct Runs
{
    Runs();
    Runs(qint64, qint64);
    //длина отрезка
    qint64 len;
    //кластер первого элемента
    qint64 first;
};

#endif // RUNS_H
