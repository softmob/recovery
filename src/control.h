#ifndef CONTROL_H
#define CONTROL_H

#include <QObject>
#include <memory>
#include <qt_windows.h>
#include "init.h"
#include "mainwindow.h"
#include "file.h"
#include <QDebug>
#include <QProgressDialog>

class MainWindow;

class Control : public QObject
{
    Q_OBJECT
public:
    void startApp();

    static Control& Instance()
    {
        static Control theSingleInstance;
        return theSingleInstance;
    }
    QVector<File> setScan(char);
private:
    QVector<File> search();
private:

private:
    Control(){}
    Control(const Control& root);
    Control& operator=(const Control&);
    std::unique_ptr<MainWindow> start_ptr;
};

#endif // CONTROL_H
