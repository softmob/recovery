#include "titlenoresidentatt.h"

QDataStream & operator>> ( QDataStream & in, TitleNoResidentAtt& value)
{
    in >> value.vcn >>
          value.vcn_last >>
          value.off >>
          value.rar >>
          value._nop >>
          value.data_size >>
          value.data_size_real >>
          value.data_stream;
    return in;
}
