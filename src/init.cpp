#include "init.h"

Init::Init(char oth): drive(oth)
{
    getInfo::drive = oth;
    QString path;
    path.sprintf("\\\\.\\%c:", drive);
    DiskOpen(path, hFile);
    getInfo::hFile = this->hFile;

    path.sprintf("%c:", drive);
    GetDiskFreeSpaceW(path.toStdWString().c_str(), &cluster, &sector, NULL, NULL);
    bytoToCluster = cluster * sector;

    getInfo::cluster = this->cluster;
    getInfo::sector = this->sector;
    getInfo::bytoToCluster = this->bytoToCluster;

    char *bufSector = new char[sector];
    DWORD read = 0;
    ReadFile(hFile, bufSector, sector, &read, NULL);
    setSizeFR(getByteN(bufSector+0x40, 1));

    LARGE_INTEGER beginMft;
    beginMft.QuadPart = getByteN(bufSector+0x30, 8) * bytoToCluster;
    SetFilePointerEx(hFile, beginMft, NULL, FILE_BEGIN);
    delete [] bufSector;

    char *buf = new char[sizeFR];
    read = 0;
    ReadFile(hFile, buf, sizeFR, &read, NULL);
    FileRecord mft(buf, sizeFR, false);
    MftRuns::Instance().setDrive(mft.getRunsData());
    delete [] buf;

    FileRecord setBitmap(getByteFileRecord(6), false);
}

void Init::setSizeFR(qint8 x)
{
    if (x < 0)
        sizeFR = (1 << (-x));
    else
        sizeFR = x * bytoToCluster;
    getInfo::sizeFR = this->sizeFR;
}
