#ifndef INIT_H
#define INIT_H

#include <QDataStream>
#include <memory>
#include <qt_windows.h>
#include "utility.h"
#include "title.h"
#include "filerecord.h"
#include "getinfo.h"
#include "mftruns.h"

#include <QDebug>

class Init
{
public:
    Init(char);
private:
    void setSizeFR(qint8);
private:
    char drive;
    long unsigned int  cluster;
    long unsigned int  sector;
    long long unsigned int  bytoToCluster;
    HANDLE hFile;
    quint32 sizeFR;
};

#endif // INIT_H
