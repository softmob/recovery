#ifndef TITLEMFT_H
#define TITLEMFT_H

#include <QDataStream>
#include <QString>
#include "utility.h"

struct TitleMft
{
    //'FILE'	Сигнатура, признак записи MFT
    QString Signature;
    //04h	2 	2Ah	Смещение начала массива восстановления
    quint16 arr_off;
    //06h	2	3	Размер массива восстановления в словах
    quint16 arr_size;
    //08h	8	12345678h	Номер первой записи в файле $LogFile, описывающей изменение этой записи
    quint64 num_logfile;
    // 10h	2	1	Последовательный номер описываемого файла (наверное)(Record Sequence Number)
    quint16 num_record;
    // 12h	2	1	Количество ссылок на эту запись
    quint16 hard_link;
    // 14h	2	30h	Смещение начала списка атрибутов файла
    quint16 off_first_att;
    /*16h	2	0	Флаги
    биты :
     0 - Запись сейчас обновляется
     1 - Это каталог, а не файл
     2 - ?
     3 - ?*/
    quint16 flag;
    //18h	4	2F0h	Число байтов, использованных в этой записи
    quint32 byte_use;
    //1Ch	4	400h	Байт доступно в этой записи
    quint32 byte_free;
    //20h	8	0	Структура FileReference, описывающая базовую запись файла. Содержит 0, если запись базовая
    quint64 file_ref;
    // идентификатор следующего атрибута
    quint16 next_id;
    //2Ah	2	XP	для выравнивания
    quint16 _alignment;
    //2Ch	4	XP	индекс данной файловой записи (number of this MFT record)
    quint32 index;
};

QDataStream & operator>> ( QDataStream&, TitleMft&);

#endif // TITLEMFT_H
