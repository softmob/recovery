#include "utility.h"
#include "filerecord.h"

QString getStringASCII(QDataStream & in, quint16 size)
{
    QString ret;
    ret.resize(size);
    for (quint16 i = 0; i < size; ++i)
    {
        quint8 ch;
        in >> ch;
        ret[i] = ch;
    }
    return ret;
}

QString getStringUTF16(QDataStream & in, quint16 size)
{
    QString ret;
    ret.resize(size);
    for (quint16 i = 0; i < size; ++i)
    {
        QChar ch;
        in >> ch;
        ret[i] = ch;
    }
    return ret;
}

quint64 getByteN(QDataStream &in, quint8 len)
{
    quint64 res = 0;
    for (quint8 i = 0; i < len; ++i)
    {
        quint8 byte;
        in >> byte;
        res = (1 << 8 * i) * byte + res;
    }
    return res;
}


quint64 getByteN(const char * str, quint8 len)
{
    quint64 res = 0;
    for (quint8 i = 0; i < len; ++i)
    {
        res = (1 << 8 * i) * str[i] + res;
    }
    return res;
}


bool DiskOpen(QString path, HANDLE& hFile)
{
    hFile = CreateFile(path.toStdWString().c_str(),          // drive to open
                       GENERIC_READ | GENERIC_WRITE,                // no access to the drive
                       FILE_SHARE_READ | FILE_SHARE_WRITE, // share mode
                       NULL,             // default security attributes
                       OPEN_EXISTING,    // disposition
                       0,                // file attributes
                       NULL);         // do not copy file attributes
    if ( GetLastError() != 0)
        throw "heldle";
    return hFile != INVALID_HANDLE_VALUE;
}


void DiskClose(HANDLE& hFile)
{
    if (hFile != INVALID_HANDLE_VALUE)
        CloseHandle(hFile);
}


#include <QDebug>
QVector<DriveInfo> getDriveNTFS()
{
    QVector<DriveInfo> ret;
    TCHAR str[0x100];
    QFileInfoList q = QDir::drives();
    for (int i = 0; i < q.size(); ++i)
    {
        // if (q[i].isReadable())
        {
            GetVolumeInformation(q[i].path().toStdWString().c_str(), NULL,0, NULL, NULL, NULL, str, 0x100);
            if (QString::fromWCharArray(str) == QString("NTFS"))
            {
                UINT Dtype = GetDriveType(q[i].path().toStdWString().c_str());
                DriveInfo tmp;
                tmp.drive = q[i].path();
                tmp.drive.replace("/", "");
                tmp.type = Dtype;
                ret.push_back(tmp);
            }
        }
    }
    return ret;
}

QString getSystemDrive()
{
    int sz = GetWindowsDirectory(NULL, 0);
    TCHAR *dr = new TCHAR[sz];
    GetWindowsDirectory(dr, sz);
    QString ret =  QString::fromWCharArray(dr);
    delete [] dr;
    return ret.mid(0, 2);
}


quint64 FileRefToId(quint64 val)
{
    return val & quint64(0x0000FFFFFFFFFFFFull);
}

LARGE_INTEGER GetFilePointerEx (HANDLE hFile)
{
    LARGE_INTEGER liOfs = {0, 0};
    LARGE_INTEGER liNew = {0, 0};
    SetFilePointerEx(hFile, liOfs, &liNew, FILE_CURRENT);
    return liNew;
}

QByteArray getByteFileRecord(quint64 i)
{
    char *buf = new char[getInfo::sizeFR];
    DWORD read = 0;
    quint64 off = MftRuns::Instance().getOffset(i);
    LARGE_INTEGER offset;
    offset.QuadPart = off;
    SetFilePointerEx(getInfo::hFile, offset, NULL, FILE_BEGIN);
    ReadFile(getInfo::hFile, buf, getInfo::sizeFR, &read, NULL);
    QByteArray res(buf, read);
    delete [] buf;
    return res;
}

QByteArray getSector(quint64 off)
{
    char *buf = new char[getInfo::sector];
    DWORD read = 0;
    LARGE_INTEGER offset;
    offset.QuadPart = off;
    SetFilePointerEx(getInfo::hFile, offset, NULL, FILE_BEGIN);
    ReadFile(getInfo::hFile, buf, getInfo::sector, &read, NULL);
    QByteArray res(buf, read);
    delete [] buf;
    return res;
}

QByteArray getCluster(quint64 off)
{
    char *buf = new char[getInfo::bytoToCluster];
    DWORD read = 0;
    LARGE_INTEGER offset;
    offset.QuadPart = off;
    SetFilePointerEx(getInfo::hFile, offset, NULL, FILE_BEGIN);
    ReadFile(getInfo::hFile, buf, getInfo::bytoToCluster, &read, NULL);
    QByteArray res(buf, read);
    delete [] buf;
    return res;
}


bool valueMftBitMap(quint32 n)
{
    quint32 start = n / 8;
    quint8 val = getInfo::MftBitmap[start];
    return getBitInByte(val, n - start * 8);
}

bool valueBitMap(quint32 n)
{
    quint32 start = n / 8;
    quint8 val = getInfo::Bitmap[start];
    return getBitInByte(val, n - start * 8);
}

bool getBitInByte(quint8 val, quint8 i)
{
    return (val & (1 << i)) != 0;
}

QString ConvertFileSize(quint64 val)
{
    const wchar_t *s[] = {L"байт", L"КБ", L"МБ", L"ГБ", L"ТБ"};
    int lev = 0;
    while (val > 10000)
    {
        val /= 1024;
        ++lev;
    }
    return QString::number(val) + " " + QString::fromWCharArray(s[lev]);
}

QString convertToUtc(quint64 val)
{
    quint64 utc = 11644473600000 * 10000;
    val -= utc;
    val /= 10000000;
    QDateTime tmp;
    tmp.setTime_t(val);
    return tmp.toString("dd.MM.yyyy hh:mm");
}

std::pair<quint64, quint64> checkClusterFile(QVector<Runs> &v)
{
    quint64 free = 0, sum = 0;
    for (int i = 0; i < v.size(); ++i)
    {
        for (qint64 j = 0; j < v[i].len; ++j)
        {
            free += (valueBitMap(v[i].first + j) == false);
        }
        sum += v[i].len;
    }
    if (v.empty())
        free = sum = 1;
    return std::make_pair(free, sum);
}


QString statusFile(std::pair<quint64, quint64> q)
{
    if (q.first == q.second)
        return "Отличное";
    else if (q.first == 0)
        return "Утрачен";
    else if (q.first >= q.second / 2)
        return "Среднее";
    else
        return "Плохое";
}


QString hexToString(QByteArray & arr)
{
    QString res;
    for (auto &x: arr)
    {
        res += QString::number(x, 16) + " ";
    }
    return res;
}

bool recoveryFile(File x, QString dir,  QProgressDialog& pprd)
{
    bool res = true;
    QString path = dir + "/" + x.name;
    // qDebug() << path;
    QFile write_file(path);
    if (write_file.open(QIODevice::WriteOnly))
    {
        if (!x.data_byte.isEmpty())
        {
            write_file.write(x.data_byte);
            pprd.setValue(pprd.value()+1);
        }
        else
        {
            for (int i = 0; i < x.data_list.size(); ++i)
            {
                qint64 off = x.data_list[i].first;
                for (qint64 j = 0; j < x.data_list[i].len; ++j)
                {
                    write_file.write( getCluster((off + j) * getInfo::bytoToCluster) );

                    qApp->processEvents();
                    if (pprd.wasCanceled())
                    {
                        write_file.remove();
                        return false;
                    }
                    pprd.setValue(pprd.value()+1);
                }
            }
        }
    }
    else
    {
        res = false;
    }
    write_file.close();
    return res;
}


quint64 getDriveSize(char drive)
{
    ULARGE_INTEGER size;
    QString path;
    path.sprintf("%c:", drive);
    GetDiskFreeSpaceEx(path.toStdWString().c_str(), NULL, &size, NULL);
    return size.QuadPart;
}
