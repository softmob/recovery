#include "widgetheadhex.h"
#include "ui_widgetheadhex.h"

WidgetHeadHex::WidgetHeadHex(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetHeadHex)
{
    ui->setupUi(this);
}

WidgetHeadHex::~WidgetHeadHex()
{
    delete ui;
}

QTextBrowser *WidgetHeadHex::getTextBrowser()
{
    return  ui->textBrowser;
}
