#include "titleatt.h"

QDataStream & operator>> (QDataStream & in, TitleAtt& value)
{
    in >> value.type;
    if (value.type != 0x0FFFFFFFF)
    {
        in >> value.size >> value.flag >> value.len
           >> value.off >> value.rar >> value.num;
    }
    return in;
}
