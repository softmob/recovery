#include "control.h"

void Control::startApp()
{
    start_ptr.reset(new MainWindow());
    start_ptr.get()->show();
}

QVector<File> Control::setScan(char drive)
{
    if (getInfo::hFile != INVALID_HANDLE_VALUE)
        DiskClose(getInfo::hFile);
    Init val(drive);
    return search();
}

QVector<File> Control::search()
{
    QVector<File> res;
    quint32 size = MftRuns::Instance().size(), mtfValid = 0;

    for (quint32 i = 0; i < size; ++i)
    {
        if (valueMftBitMap(i) == true)
            continue;
        ++mtfValid;
    }

    QProgressDialog pprd("Идет процесс обработки данных...", "Отмена", 0, mtfValid, start_ptr.get(), Qt::Window);
    pprd.setWindowModality(Qt::WindowModal);
    pprd.resize(300, 100);
    pprd.setMaximumSize(pprd.size());
    pprd.setMinimumDuration(0);
    pprd.setWindowTitle("Пожалуйста подождите");
    pprd.setValue(0);
    mtfValid = 0;

    for (quint32 i = 0; i < size; ++i)
    {
        if (valueMftBitMap(i) == true)
            continue;

        qApp->processEvents();
        if (pprd.wasCanceled())
        {
            break;
        }
        pprd.setValue(++mtfValid);

        FileRecord cur(getByteFileRecord(i));
        if (!cur.isRight() || cur.getName().isEmpty())
            continue;
        File tmp;
        tmp.name = cur.getName();
        tmp.path = cur.getPath();
        tmp.data_list = cur.getRunsData();
        tmp.data_byte = cur.getByteData();
        tmp.length = cur.getLength();
        tmp.time = convertToUtc(cur.getTime());
        res.push_back(tmp);
    }

    return res;
}
