#include "titlemft.h"

QDataStream & operator>> ( QDataStream & in, TitleMft& value)
{
    value.Signature = getStringASCII(in, 4);
    in >>
          value.arr_off >>
          value.arr_size >>
          value.num_logfile >>
          value.num_record >>
          value.hard_link >>
          value.off_first_att >>
          value.flag >>
          value.byte_use >>
          value.byte_free >>
          value.file_ref >>
          value.next_id >>
          value._alignment >>
          value.index;
    return in;
}
