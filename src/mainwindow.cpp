#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->exit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
    connect(ui->about, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->help, SIGNAL(triggered()), this, SLOT(help()));

    connect(ui->tabWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(on_table_customContextMenuRequested(QPoint)));

    ui->splitter->setStretchFactor(0, 3);
    ui->splitter->setStretchFactor(1, 1);

    ui->tabWidget->setTabText(0, "Сводка");
    ui->tabWidget->setTabText(1, "Заголовок");
    
    QFileSystemModel *m = new QFileSystemModel(this);
    QVector<DriveInfo> dr = getDriveNTFS();
    ui->table->verticalHeader()->setHidden(true);
    for (int i = 0; i < dr.size(); ++i)
    {
        if (m->index(dr[i].drive).isValid())
            ui->comboBox->addItem(m->fileIcon(m->index(dr[i].drive)), dr[i].drive);
    }
    ui->splitter->setChildrenCollapsible(false);

    auto f = ui->filter->textMargins();
    f.setLeft(16);
    ui->filter->setTextMargins(f);
    on_comboBox_activated(ui->comboBox->itemText(0));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::tableFill(QVector<File> list)
{    
    ui->table->clearContents();
    ui->table->setRowCount(0);
    map.clear();
    ui->table->setSortingEnabled(false);
    for (File &x: list)
    {
        ui->table->insertRow(ui->table->rowCount());
        
        QTableWidgetItem *nameItem = new QTableWidgetItem(x.name);
        nameItem->setCheckState(Qt::Unchecked);
        map[nameItem] = x;

        ui->table->setItem(ui->table->rowCount()-1, 0, nameItem);
        QTableWidgetItem *pathItem = new QTableWidgetItem(x.path);
        ui->table->setItem(ui->table->rowCount()-1, 1, pathItem);
        
        QTableWidgetItem *sizeItem = new QTableWidgetItem(ConvertFileSize(x.length));
        ui->table->setItem(ui->table->rowCount()-1, 2, sizeItem);
        
        QTableWidgetItem *timeItem = new QTableWidgetItem(x.time);
        ui->table->setItem(ui->table->rowCount()-1, 3, timeItem);
        
        
        auto st = checkClusterFile(x.data_list);
        QTableWidgetItem *noteItem = new QTableWidgetItem(QString("Сохранено ")
                                                          + QString::number(st.first)
                                                          +  QString(" из ")
                                                          + QString::number(st.second)
                                                          + QString(" фрагментов"));
        ui->table->setItem(ui->table->rowCount()-1, 5, noteItem);
        
        QTableWidgetItem *statusItem = new QTableWidgetItem(statusFile(st));
        ui->table->setItem(ui->table->rowCount()-1, 4, statusItem);
    }
    ui->table->setSortingEnabled(true);
    //qDebug() << "bitmap: " << getInfo::Bitmap.size();
}

void MainWindow::recovery(QList<QTableWidgetItem *> list)
{
    if (list.isEmpty())
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("Действие завершено");
        msgBox.setText("Нечего не выбаанно");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        return ;
    }
    QList<File> v;
    for (auto &x: list)
    {
        v.push_back(map[x]);
    }

    QString dir = QFileDialog::getExistingDirectory(
                this, QString(),
                QDir::rootPath(),
                QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks
                );

    if (dir.at(0).toUpper().toLatin1() == toupper(getInfo::drive))
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("Предупреждение");
        msgBox.setText("Восстановление файла на исходный диск может быть не безопастным.");
        msgBox.setInformativeText("Продолжить?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Warning);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Cancel)
            return ;
    }

    quint64 parts = 0;
    for (File &x: v)
    {
        for (int i = 0; i < x.data_list.size(); ++i)
            parts += x.data_list[i].len;
        if (x.data_list.empty() && !x.data_byte.isEmpty())
            ++parts;
    }

    QProgressDialog pprd("Идет восстановление данных...", "Отмена", 0, parts, this, Qt::Window);
    pprd.setWindowModality(Qt::WindowModal);
    pprd.resize(300, 100);
    pprd.setMaximumSize(pprd.size());
    pprd.setMinimumDuration(0);
    pprd.setWindowTitle("Пожалуйста подождите");

    int sum = 0;
    QTime timer;
    timer.start();
    for (File &x: v)
    {
        sum += recoveryFile(x, dir, pprd);
    }
    pprd.close();
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Действие завершено");
    msgBox.setText("Файлов восстановлено: " + QString::number(sum));
    msgBox.setInformativeText("Операция заняла: " + QString::number(timer.elapsed()) + " мс" );
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void MainWindow::about()
{
    QWidget tmp(this);
    tmp.setWindowIcon(QPixmap(":/sys/images/About.ico"));
    QMessageBox::about(&tmp, tr("О программе"),
                       tr("<p>Восстановление удаленных файлов"
                          "<p>Проект разработан в рамках курсовой работы 5 семестра"
                          "<p>По дисциплине СПО"
                          "<p>Автор: Наумов Андрей"
                          ));
}

void MainWindow::help()
{
    QWidget tmp(this);
    tmp.setWindowIcon(QPixmap(":/sys/images/Help book.ico"));
    QMessageBox::about(&tmp, tr("Справка"),
                       tr("<p>Для получения руководства пользователя обратитесь к пояснительной записке."
                          ));
}

void MainWindow::recoverySelect()
{
    QList<QTableWidgetItem*> list = ui->table->selectedItems();
    QList<QTableWidgetItem*> res;
    for (QTableWidgetItem* &x: list)
    {
        if (x->column() == 0)
            res.push_back(x);
    }
    recovery(res);
}

void MainWindow::recoveryChecked()
{
    QList<QTableWidgetItem*> res;
    int n = ui->table->rowCount();
    for (int i = 0; i < n; ++i)
    {
        if (ui->table->item(i, 0)->checkState() == Qt::Checked)
        {
            res.push_back(ui->table->item(i, 0));
        }
    }
    recovery(res);
}

void MainWindow::selectAdd()
{
    QList<QTableWidgetItem*> list = ui->table->selectedItems();
    QSet<int> r;
    for (QTableWidgetItem* &x: list)
        r.insert(x->row());
    for (auto &x: r)
        ui->table->item(x, 0)->setCheckState(Qt::Checked);
}

void MainWindow::selectDel()
{
    QList<QTableWidgetItem*> list = ui->table->selectedItems();
    QSet<int> r;
    for (QTableWidgetItem* &x: list)
        r.insert(x->row());
    for (auto &x: r)
        ui->table->item(x, 0)->setCheckState(Qt::Unchecked);
}

void MainWindow::selectAllUncheck()
{
    int n = ui->table->rowCount();
    for (int i = 0; i < n; ++i)
        ui->table->item(i, 0)->setCheckState(Qt::Unchecked);
}

void MainWindow::selectAllCheck()
{
    int n = ui->table->rowCount();
    for (int i = 0; i < n; ++i)
        ui->table->item(i, 0)->setCheckState(Qt::Checked);
}

void MainWindow::on_pushScan_clicked()
{    
    ui->table->setRowCount(0);
    char drive = ui->comboBox->currentText().at(0).toLatin1();
    tableFill(Control::Instance().setScan(drive));
    on_filter_textEdited(ui->filter->text());
}

void MainWindow::on_table_doubleClicked(const QModelIndex &index)
{
    auto item = ui->table->item(index.row(), 0);
    if (item->checkState() == Qt::Unchecked)
        item->setCheckState(Qt::Checked);
    else
        item->setCheckState(Qt::Unchecked);
}

void MainWindow::on_table_itemSelectionChanged()
{ 
    ui->tab->getTextBrowser()->clear();
    ui->tab_2->getTextBrowser()->clear();

    QList<QTableWidgetItem*> list = ui->table->selectedItems();
    QSet<int> r;
    for (QTableWidgetItem* &x: list)
        r.insert(x->row());
    if (r.size() > 1)
    {
        ui->tab->getTextBrowser()->setText("Выбрано несколько файлов");
    }
    else if (r.size() == 1)
    {
        QString res;
        res += "Имя файла: " + list.at(0)->text() + "\n";
        res += "Путь: " + list.at(1)->text() + "\n\n";
        res += "Размер: " + list.at(2)->text() + "\n\n";
        res += "Состояние: " + list.at(4)->text() + "\n\n";
        res += "Изменён: " + list.at(3)->text() + "\n";
        ui->tab->getTextBrowser()->setText(res);
        auto file = map[list.at(0)];
        if (!file.data_byte.isEmpty())
        {
            // ui->tab_2->getTextBrowser()->setText(hexToString(file.data_byte));

            ui->tab_2->getTextBrowser()->setText(QString(file.data_byte.toHex()).toUpper());
        }
        else if (!file.data_list.empty())
        {
            // ui->tab_2->getTextBrowser()->setText(getSector(file.data_list.at(0).first * getInfo::bytoToCluster));
            ui->tab_2->getTextBrowser()->setText(QString(getSector(file.data_list.at(0).first * getInfo::bytoToCluster).toHex()).toUpper());
        }
    }
}

void MainWindow::on_table_customContextMenuRequested(const QPoint &pos)
{
    if (ui->table->rowCount() == 0)
        return ;
    // получаем глобальные координаты
    QPoint globalPos;
    // если запрос от QAbstractScrollArea
    if (sender()->inherits("QAbstractScrollArea"))
        globalPos = ((QAbstractScrollArea*)sender())->viewport()->mapToGlobal(pos);
    // если от других виджетов
    else
        globalPos = ((QWidget*)sender())->mapToGlobal(pos);

    // Создаем меню
    QMenu menu;
    /* // Создаем пункт меню
    QAction *action1 = new QAction(QString::fromUtf8("Пункт 1"), this);
    // соединяемся со слотом выбора этого пункта меню
    //connect(action, SIGNAL(triggered()), this, SLOT(action()));
    // добавляем пункт в меню
    menu.addAction(action1);
    // добавляем разделитель
    menu.addSeparator();
    // добавляем еще один пункт меню
    QAction *action2 = new QAction(QString::fromUtf8("Пункт 2"), this);
    menu.addAction(action2);*/


    QAction *action1 = new QAction("Восстановить выделенные..", this);
    connect(action1, SIGNAL(triggered()), this, SLOT(recoverySelect()));
    menu.addAction(action1);
    QAction *action2 = new QAction("Восстановить отмеченные..", this);
    connect(action2, SIGNAL(triggered()), this, SLOT(recoveryChecked()));
    menu.addAction(action2);
    menu.addSeparator();
    QAction *action3 = new QAction("Выделить", this);
    connect(action3, SIGNAL(triggered()), this, SLOT(selectAdd()));
    menu.addAction(action3);
    QAction *action4 = new QAction("Снять выделение", this);
    connect(action4, SIGNAL(triggered()), this, SLOT(selectDel()));
    menu.addAction(action4);
    menu.addSeparator();
    QAction *action5 = new QAction("Выделить все", this);
    connect(action5, SIGNAL(triggered()), this, SLOT(selectAllCheck()));
    menu.addAction(action5);
    QAction *action6 = new QAction("Снять выделение со всех", this);
    connect(action6, SIGNAL(triggered()), this, SLOT(selectAllUncheck()));
    menu.addAction(action6);

    // отображаем меню и обрабатываем пользовательский выбор
    QAction* selectedItem = menu.exec(globalPos);
    // что-то было выбрано
    /*if (selectedItem)
    {
        QMessageBox::warning(this, "", QString::fromUtf8("Выбран %1").arg(selectedItem->text()));
    }
    // пользователь ничего не выбрал
    else
    {
        QMessageBox::warning(this, "", QString::fromUtf8("Ничего не выбрано"));
    }*/
}

void MainWindow::on_pushRecovery_clicked()
{
    recoveryChecked();
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    ui->label_info->setText(
                "[" + arg1 + "] NTFS; "
                + ConvertFileSize(getDriveSize(arg1.at(0).toLatin1()))
                );
}

void MainWindow::on_filter_textEdited(const QString &arg1)
{
    int r = ui->table->rowCount();
    if (arg1.isEmpty())
    {
        for (int i = 0; i < r; ++i)
        {
            ui->table->setRowHidden(i, false);
        }
    }
    QRegExp re(arg1);
    for (int i = 0; i < r; ++i)
    {
        QString name = ui->table->item(i, 0)->text();
        bool f = (re.indexIn(name) == -1);
        if (f)
            ui->table->item(i, 0)->setCheckState(Qt::Unchecked);
        ui->table->setRowHidden(i, f);
    }
}
